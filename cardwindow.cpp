/*
    This file is part of the KhalkhiCards program, part of the KDE project.

    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

// qt specific
#include <qlayout.h>
#include <qpopupmenu.h>
#include <qlabel.h>
// kde specific
#include <kapp.h>
#include <klocale.h>
#include <kiconloader.h>
// lib specific
#include "cardwindow.h"


static const char ServerContextId[] = "KhalkhiCards";

static const int PhotoWidthPixels = 50;
static const int PhotoHeightPixels = 70;


namespace Khalkhi {

CardWindow::CardWindow( const KABC::Addressee &P )
: CardView()
{
    ServicesMenuFiller.setContext( ServerContextId );

    setWFlags( WType_TopLevel | WStyle_Customize | WStyle_NormalBorder | WDestructiveClose );
    setBackgroundMode( PaletteBase, PaletteBase );

    setPerson( P );
    setAcceptDrops( true );
}


QPopupMenu *CardWindow::createPopupMenu( const QPoint& Pos )
{
    // for card content
    QPopupMenu *Menu = CardView::createPopupMenu( Pos );

    ServicesMenuFiller.set( Person );
    ServicesMenuFiller.fillMenu( Menu, Menu->count() );

    return Menu;
}


void CardWindow::updateView()
{
    CardView::updateView();
    // window frame
    setCaption( Person.realName() );
    const QImage Image = Person.photo().data();

    QPixmap Photo;
    if( Image.isNull() )
        Photo = KGlobal::iconLoader()->loadIcon( "personal", KIcon::NoGroup, PhotoWidthPixels );
    else
        Photo = Image.smoothScale( PhotoWidthPixels, PhotoHeightPixels, QImage::ScaleMin );

    setIcon( Photo );
}


// sigh, sync() does not give the width
// so we have to catch the calculation here and resize to gain a perfect card
void CardWindow::showEvent( QShowEvent *SE )
{
    CardView::showEvent( SE );
    resize( contentsWidth(), contentsHeight() );
}


CardWindow::~CardWindow()
{
    emit closed( Person );
}

}

#include "cardwindow.moc"
