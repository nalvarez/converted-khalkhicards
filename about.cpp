/*
    This file is part of the KhalkhiCards program, part of the KDE project.

    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/


// program specific
#include "about.h"


// Program
static const char ProgramId[] =          "khalkhicards";
static const char ProgramName[] =        I18N_NOOP("KhalkhiCards");
static const char ProgramDescription[] = I18N_NOOP("Viewer for Person Cards");
static const char ProgramVersion[] =     "0.2.1";
static const char ProgramCopyright[] =   "(C) 2006-2007 Friedrich W. H. Kossebau";
static const char ProgramComment[] =     I18N_NOOP("KhalkhiCards shows interactive cards of persons in your addressbook");
// Author
static const char FWHKName[] =         "Friedrich W. H. Kossebau";
static const char FWHKTask[] =         I18N_NOOP("Author");
static const char FWHKEmailAddress[] = "kossebau@kde.org";


KhalkhiCardsAboutData::KhalkhiCardsAboutData()
: KAboutData( ProgramId, ProgramName, ProgramVersion, ProgramDescription,
              KAboutData::License_GPL_V2, ProgramCopyright, ProgramComment, 0, FWHKEmailAddress )
{
    addAuthor( FWHKName, FWHKTask, FWHKEmailAddress );
}
