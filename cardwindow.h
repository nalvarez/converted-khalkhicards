/*
    This file is part of the KhalkhiCards program, part of the KDE project.

    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/


#ifndef CARDWINDOW_H
#define CARDWINDOW_H


// kde specific
#include <kabc/addressee.h>
// Khalkhi specific
#include <actionservicemenufiller.h>
#include <cardview.h>


using namespace KABC;

namespace Khalkhi {

class CardWindow : public CardView
{
  Q_OBJECT

public:
    CardWindow( const KABC::Addressee &Person );
    virtual ~CardWindow();

public: // CardView API
    virtual void updateView();

public: // QWidget API
    virtual void showEvent( QShowEvent *SE );

protected: // QTextEdit API
    virtual QPopupMenu *createPopupMenu( const QPoint& Pos );

signals:
    void closed( const Addressee &Person );

protected:
    ActionServiceMenuFiller ServicesMenuFiller;
};

}

#endif
