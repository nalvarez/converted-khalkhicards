/*
    This file is part of the KhalkhiCards program, part of the KDE project.

    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

// kde specific
#include <kuniqueapplication.h>
#include <dcopclient.h>
#include <kcmdlineargs.h>
// program specific
#include "cardwindowmanager.h"
#include "program.h"


static KCmdLineOptions ProgramOptions[] =
{
  { "+[UID]", I18N_NOOP( "UID of Person to show." ), 0 },
  KCmdLineLastOption
};


KhalkhiCardsProgram::KhalkhiCardsProgram( int argc, char *argv[] )
{
    KCmdLineArgs::init( argc, argv, &AboutData );
    KCmdLineArgs::addCmdLineOptions( ProgramOptions );
    KUniqueApplication::addCmdLineOptions();
}


int KhalkhiCardsProgram::execute()
{
    // TODO: check of arguments already here?
    if( !KUniqueApplication::start() )
        return 0; //TODO: send arguments to running instance?

    // take arguments
    KCmdLineArgs *Args = KCmdLineArgs::parsedArgs();
    if( Args->count() > 0 )
    {
        for( int i=0; i<Args->count(); ++i )
        ;
    }
    Args->clear();

    KUniqueApplication KhalkhiCardsProgram;

    //if( KhalkhiCardsProgram.isRestored() )
    Khalkhi::CardWindowManager *Server = new Khalkhi::CardWindowManager;
    KhalkhiCardsProgram.dcopClient()->setDefaultObject( Server->objId() );

    return KhalkhiCardsProgram.exec();
}
