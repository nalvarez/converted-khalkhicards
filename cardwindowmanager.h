/*
    This file is part of the KhalkhiCards program, part of the KDE project.

    Copyright (C) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/

#ifndef CARDWINDOMANAGER_H
#define CARDWINDOMANAGER_H


// qt specific
#include <qdict.h>
#include <qobject.h>
// kde specific
#include <dcopobject.h>


class QTimer;
namespace KABC { class Addressee; class AddressBook; }
class CardWindow;

using namespace KABC;

namespace Khalkhi {

class CardWindowManager : public QObject, virtual public DCOPObject
{
    Q_OBJECT
    K_DCOP

public:
    CardWindowManager();
    ~CardWindowManager();

k_dcop:
    ASYNC showCard( const QString &UID );
    ASYNC closeCard( const QString &UID );

protected slots:
    /** */
    void updateCards();
    /** */
    void removeCard( const Addressee &Person );

protected:
    /** holds open cards based on uid */
    QDict<CardWindow> Cards;
    KABC::AddressBook *Book;

    /** */
    QTimer *ShutDownTimer;
};

}

#endif
