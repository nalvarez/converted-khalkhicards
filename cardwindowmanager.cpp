/*
    This file is part of the KhalkhiCards program, part of the KDE project.

    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA  02110-1301, USA.
*/


// qt specific
#include <qtimer.h>
// kde specific
#include <kabc/stdaddressbook.h>
#include <kapp.h>
#include <kwin.h>
// Khalkhi
#include <services.h>
// server specific
#include "cardwindow.h"
#include "cardwindowmanager.h"


static const int ShutDownTime = 30 * 1000;

namespace Khalkhi {

CardWindowManager::CardWindowManager()
: DCOPObject( "KhalkhiCardsIf" )
{
    ShutDownTimer = new QTimer( this );
    connect( ShutDownTimer, SIGNAL(timeout()),
             kapp, SLOT(quit()) );

    Book = StdAddressBook::self();

    connect( Book, SIGNAL(addressBookChanged(AddressBook*)), SLOT(updateCards()) );

    connect( Services::self(), SIGNAL(changed()), SLOT(updateCards()) );

    ShutDownTimer->start( ShutDownTime, true );
}


ASYNC CardWindowManager::showCard( const QString &UID )
{
    ShutDownTimer->stop();
    CardWindow* Card = Cards[UID];

    // non already there?
    if( !Card )
    {
        const KABC::Addressee Person = Book->findByUid( UID );
        if( !Person.isEmpty() )
        {
            Card = new CardWindow( Person );
            Cards.insert( UID, Card );
            connect( Card, SIGNAL(closed(const Addressee &)),
                     SLOT( removeCard(const Addressee &)) );
            Card->show();
        }
    }
    else
    {
        KWin::setOnDesktop( Card->winId(), KWin::currentDesktop() );
        Card->raise();
    }
}

ASYNC CardWindowManager::closeCard( const QString &/*UID*/ )
{
    //Cards.remove( Person.uid() );
}


void CardWindowManager::updateCards()
{
    QDictIterator<CardWindow> it( Cards );
    for( ; it.current(); ++it )
    {
        const KABC::Addressee Person = Book->findByUid( it.current()->person().uid() );

        // no longer existant?
        if( Person.isEmpty() )
            delete it.current();
        else
            it.current()->setPerson( Person );
    }
}


void CardWindowManager::removeCard( const Addressee &Person )
{
    Cards.remove( Person.uid() );

    if( Cards.isEmpty() )
        ShutDownTimer->start( ShutDownTime, true );
}


CardWindowManager::~CardWindowManager()
{
    //QDictIterator<CardWindow> it( Cards );
    //for( ; it.current(); ++it )
}

}

#include "cardwindowmanager.moc"
